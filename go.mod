module github.com/leominov/DueDateReminders

go 1.14

require (
	github.com/dustin/go-humanize v1.0.0
	github.com/hashicorp/go-cleanhttp v0.5.1
	github.com/robfig/cron/v3 v3.0.0
	github.com/sirupsen/logrus v1.6.0
	github.com/stretchr/testify v1.2.2
	gopkg.in/yaml.v2 v2.3.0
)
