# Due Date Reminders for Atlassian Jira

[![Build Status](https://travis-ci.com/leominov/DueDateReminders.svg?branch=master)](https://travis-ci.com/leominov/DueDateReminders)
[![codecov](https://codecov.io/gh/leominov/DueDateReminders/branch/master/graph/badge.svg)](https://codecov.io/gh/leominov/DueDateReminders)

![example](example.png)

See [example.reminders.yaml](example.reminders.yaml) for example.
