package main

import (
	"bytes"
	"encoding/json"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/robfig/cron/v3"
	"github.com/sirupsen/logrus"
)

type DueDateReminders struct {
	config  *Configuration
	httpCli *http.Client
	cronCli *cron.Cron
	jiraCli JiraClient
}

func NewDueDateReminders(cfg *Configuration, httpCli *http.Client) (*DueDateReminders, error) {
	cronCli := cron.New(cron.WithSeconds())
	jiraCli := NewJiraClient(cfg.Jira.Host, httpCli)
	jiraCli.SetCredentials(cfg.Jira.Username, cfg.Jira.Password)
	r := &DueDateReminders{
		config:  cfg,
		httpCli: httpCli,
		cronCli: cronCli,
		jiraCli: jiraCli,
	}
	_, err := r.cronCli.AddFunc(cfg.Schedule, r.run)
	return r, err
}

func (d *DueDateReminders) Start() {
	d.cronCli.Start()
}

func (d *DueDateReminders) Stop() {
	d.cronCli.Stop()
}

func (d *DueDateReminders) run() {
	logrus.Println("Do the hustle")
	issues, err := d.jiraCli.GetIssues(d.config.Jira.JQL)
	if err != nil {
		logrus.Error(err)
		return
	}
	if len(issues) == 0 {
		return
	}
	expired, forthcoming := issuesSummary(issues)
	if d.config.Slack.Enabled {
		err := d.notifySlack(issues, expired, forthcoming)
		if err != nil {
			logrus.Error(err)
		}
	}
}

func issuesSummary(issues []*Issue) (expired, forthcoming int) {
	for _, issue := range issues {
		dueDate, err := ParseDueDate(issue.Fields.DueDate)
		if err != nil {
			continue
		}
		if time.Since(dueDate) < 0 {
			forthcoming++
		} else {
			expired++
		}
	}
	return
}

func (d *DueDateReminders) RenderSlackTemplate(issues []*Issue) string {
	text := ""
	for _, issue := range issues {
		data := map[string]interface{}{
			"Host":  d.config.Jira.Host,
			"Issue": issue,
		}
		item, err := gotmpl(d.config.Slack.Template, data)
		if err != nil {
			logrus.Errorf("Failed to render template of %s issue: %v", issue.Key, err)
			continue
		}
		item = strings.TrimSpace(item)
		if len(item) == 0 {
			continue
		}
		text += item + "\n\n"
	}
	text = strings.TrimSpace(text)
	return text
}

func (d *DueDateReminders) notifySlack(issues []*Issue, expired, forthcoming int) error {
	text := d.RenderSlackTemplate(issues)
	if d.config.DryRun {
		logrus.Info(text)
		return nil
	}
	req := d.config.Slack.SlackRequest
	req.Attachments = []SlackAttachment{
		{
			Text:     text,
			Color:    d.config.Slack.Color,
			MrkdwnIn: []string{"fallback", "pretext", "text"},
			Footer:   d.config.Slack.Footer,
			Fields: []Field{
				{
					Title: "Forthcoming",
					Value: strconv.Itoa(forthcoming),
					Short: true,
				},
				{
					Title: "Expired",
					Value: strconv.Itoa(expired),
					Short: true,
				},
			},
		},
	}
	var buf bytes.Buffer
	if err := json.NewEncoder(&buf).Encode(req); err != nil {
		return err
	}
	resp, err := d.httpCli.Post(d.config.Slack.APIURL, "application/json", &buf)
	if err != nil {
		return err
	}
	return resp.Body.Close()
}
