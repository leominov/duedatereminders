FROM golang:1.14-alpine3.11 as builder
WORKDIR /go/src/github.com/leominov/duedatereminders
COPY . .
RUN go build -o duedatereminders ./

FROM alpine:3.11
COPY --from=builder /go/src/github.com/leominov/duedatereminders/duedatereminders /usr/local/bin/duedatereminders
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
ENTRYPOINT ["duedatereminders"]
