package main

import (
	"sort"
	"strings"
	"time"

	humanize "github.com/dustin/go-humanize"
)

type Issue struct {
	ID     string `json:"id"`
	Self   string `json:"self"`
	Expand string `json:"expand"`
	Key    string `json:"key"`
	Fields Fields `json:"fields"`
}

type Fields struct {
	Labels     []string     `json:"labels"`
	Summary    string       `json:"summary"`
	DueDate    string       `json:"duedate"`
	Status     *Status      `json:"status"`
	Assignee   *User        `json:"assignee"`
	Creator    *User        `json:"creator"`
	Reporter   *User        `json:"reporter"`
	Priority   *Priority    `json:"priority"`
	Project    *Project     `json:"project"`
	Components []*Component `json:"components"`
}

type Component struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

type Project struct {
	Self string `json:"self"`
	ID   string `json:"id"`
	Key  string `json:"key"`
	Name string `json:"name"`
}

type User struct {
	Self string `json:"self"`
	Name string `json:"name"`
	Key  string `json:"key"`
}

type Priority struct {
	Self string `json:"self"`
	Name string `json:"name"`
	ID   string `json:"id"`
}

type Status struct {
	ID             string          `json:"id"`
	Self           string          `json:"self"`
	Description    string          `json:"description"`
	IconURL        string          `json:"iconUrl"`
	Name           string          `json:"name"`
	StatusCategory *StatusCategory `json:"statusCategory"`
}

type StatusCategory struct {
	ID        int    `json:"id"`
	Self      string `json:"self"`
	Key       string `json:"key"`
	ColorName string `json:"colorName"`
	Name      string `json:"name"`
}

func ParseDueDate(dueDate string) (time.Time, error) {
	date, err := time.Parse("2006-01-02", dueDate)
	if err != nil {
		return time.Time{}, err
	}
	return date, nil
}

func (i *Issue) GetHumanizeDueDate() string {
	dueDate, err := ParseDueDate(i.Fields.DueDate)
	if err != nil {
		return "?"
	}
	return humanize.Time(dueDate)
}

func (i *Issue) GetLabels() string {
	labels := i.Fields.Labels
	if i.Fields.Components != nil {
		for _, component := range i.Fields.Components {
			labels = append(labels, component.Name)
		}
	}
	sort.Strings(labels)
	result := strings.Join(labels, ", ")
	if len(result) > 0 {
		result = " (" + result + ")"
	}
	return result
}

type SlackRequest struct {
	Text        string            `json:"text,omitempty" yaml:"text,omitempty"`
	Channel     string            `json:"channel,omitempty" yaml:"channel"`
	Username    string            `json:"username,omitempty" yaml:"username,omitempty"`
	IconEmoji   string            `json:"icon_emoji,omitempty" yaml:"icon_emoji,omitempty"`
	IconURL     string            `json:"icon_url,omitempty" yaml:"icon_url,omitempty"`
	LinkNames   bool              `json:"link_names,omitempty" yaml:"link_names,omitempty"`
	Attachments []SlackAttachment `json:"attachments" yaml:"-"`
}

// https://api.slack.com/docs/message-attachments#attachment_structure
type SlackAttachment struct {
	Title      string   `json:"title,omitempty"`
	TitleLink  string   `json:"title_link,omitempty"`
	AuthorName string   `json:"author_name,omitempty"`
	AuthorLink string   `json:"author_link,omitempty"`
	AuthorIcon string   `json:"author_icon,omitempty"`
	Color      string   `json:"color,omitempty"`
	PreText    string   `json:"pretext,omitempty"`
	Text       string   `json:"text"`
	MrkdwnIn   []string `json:"mrkdwn_in,omitempty"`
	Footer     string   `json:"footer,omitempty"`
	FooterIcon string   `json:"footer_icon,omitempty"`
	Fields     []Field  `json:"fields,omitempty"`
}

type Field struct {
	Title string `json:"title"`
	Value string `json:"value"`
	Short bool   `json:"short"`
}
