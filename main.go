package main

import (
	"flag"
	"os"
	"os/signal"
	"syscall"

	"github.com/hashicorp/go-cleanhttp"

	"github.com/sirupsen/logrus"
)

var (
	configFile = flag.String("config", "/etc/duedatereminders/config.yaml", "Path to configuration file")
	dryRun     = flag.Bool("dry-run", false, "Runs without sending notifications")
)

func main() {
	flag.Parse()

	// Output logs in JSON format
	logrus.SetFormatter(&logrus.JSONFormatter{})

	cfg, err := LoadConfigurationFromFile(*configFile)
	if err != nil {
		logrus.Fatal(err)
	}
	if *dryRun {
		// Rewrite by the flag
		cfg.DryRun = true
	}

	// Rewrite JIRA creds and Slack API URL if defined
	cfg.LoadSensitiveDataFromEnv()

	cli := cleanhttp.DefaultPooledClient()
	dr, err := NewDueDateReminders(cfg, cli)
	if err != nil {
		logrus.Fatal(err)
	}

	logrus.Println("Starting duedatereminders...")
	dr.Start()

	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, syscall.SIGINT, syscall.SIGTERM)

	s := <-signalChan
	logrus.Printf("Captured %v. Exiting...", s)
	dr.Stop()
}
