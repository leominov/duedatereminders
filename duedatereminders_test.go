package main

import (
	"errors"
	"testing"
	"time"

	"github.com/hashicorp/go-cleanhttp"
	"github.com/stretchr/testify/assert"
)

func TestNewDueDateReminders(t *testing.T) {
	d, err := NewDueDateReminders(&Configuration{
		Schedule: "0 0 5 * * 1",
	}, cleanhttp.DefaultClient())
	assert.NoError(t, err)
	assert.NotNil(t, d)
}

func TestDueDateReminders_Run(t *testing.T) {
	d, err := NewDueDateReminders(&Configuration{
		Schedule: "0 0 5 * * 1",
		Slack: SlackConfig{
			Enabled: false,
		},
	}, cleanhttp.DefaultClient())
	assert.NoError(t, err)
	fakeCli := &JiraFakeClient{}
	d.jiraCli = fakeCli
	d.run()
	assert.Equal(t, 1, fakeCli.hits)
	fakeCli.err = errors.New("some error")
	d.run()
	assert.Equal(t, 2, fakeCli.hits)
	fakeCli.err = nil
	fakeCli.issues = []*Issue{
		{
			Fields: Fields{
				DueDate: time.Now().Add(-96 * time.Hour).Format("2006-01-02"),
			},
		},
		{
			Fields: Fields{
				DueDate: time.Now().Add(96 * time.Hour).Format("2006-01-02"),
			},
		},
	}
	d.run()
	assert.Equal(t, 3, fakeCli.hits)
}

func TestDueDateReminders_RenderSlackTemplate(t *testing.T) {
	c := &Configuration{
		DryRun:   false,
		Schedule: "",
		Jira: JiraConfig{
			Host: "http://127.0.0.1:9910",
		},
		Slack: SlackConfig{
			Template: `[<{{.Host}}/browse/{{.Issue.Fields.Project.Key}}|{{.Issue.Fields.Project.Key}}>] <{{.Host}}/browse/{{.Issue.Key}}|{{.Issue.Fields.Summary}}>{{.Issue.GetLabels}}
{{.Issue.GetHumanizeDueDate}} · Assignee: @{{.Issue.Fields.Assignee.Key}}`,
		},
		Template: "",
	}
	d := &DueDateReminders{
		config: c,
	}
	issues := []*Issue{
		{
			Key: "TST-1",
			Fields: Fields{
				DueDate: "1900-01-01",
				Assignee: &User{
					Key: "l.aminov",
				},
				Project: &Project{
					Key: "TST",
				},
				Components: []*Component{
					{
						Name: "INFRA",
					},
				},
				Labels: []string{
					"abc",
					"def",
				},
			},
		},
	}
	out := d.RenderSlackTemplate(issues)
	assert.Equal(t, `[<http://127.0.0.1:9910/browse/TST|TST>] <http://127.0.0.1:9910/browse/TST-1|> (INFRA, abc, def)
a long while ago · Assignee: @l.aminov`, out)
	c.Slack.Template = `{{.Slack`
	out = d.RenderSlackTemplate(issues)
	assert.Equal(t, ``, out)
}
