package main

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLoadConfigurationFromFile(t *testing.T) {
	_, err := LoadConfigurationFromFile("test_data/not-found.yaml")
	assert.Error(t, err)

	_, err = LoadConfigurationFromFile("test_data/invalid-format.yaml")
	assert.Error(t, err)

	_, err = LoadConfigurationFromFile("test_data/invalid-schedule.yaml")
	assert.Error(t, err)
	assert.Contains(t, err.Error(), "expected exactly 6 fields")

	config, err := LoadConfigurationFromFile("test_data/valid.yaml")
	assert.NoError(t, err)
	assert.Equal(t, defaultSlackUsername, config.Slack.Username)
	assert.Equal(t, defaultSlackIconEmoji, config.Slack.IconEmoji)
	assert.Equal(t, defaultSlackFooter, config.Slack.Footer)
	assert.Equal(t, defaultSlackColor, config.Slack.Color)
}

func TestConfiguration_LoadSensitiveDataFromEnv(t *testing.T) {
	c := &Configuration{}

	assert.NoError(t, os.Unsetenv("JIRA_USERNAME"))
	assert.NoError(t, os.Unsetenv("JIRA_PASSWORD"))
	assert.NoError(t, os.Unsetenv("SLACK_API_URL"))

	c.LoadSensitiveDataFromEnv()
	assert.Equal(t, "", c.Jira.Username)
	assert.Equal(t, "", c.Jira.Password)
	assert.Equal(t, "", c.Slack.APIURL)

	assert.NoError(t, os.Setenv("JIRA_USERNAME", "username"))
	assert.NoError(t, os.Setenv("JIRA_PASSWORD", "password"))
	assert.NoError(t, os.Setenv("SLACK_API_URL", "https://slack.com"))

	c.LoadSensitiveDataFromEnv()
	assert.Equal(t, "username", c.Jira.Username)
	assert.Equal(t, "password", c.Jira.Password)
	assert.Equal(t, "https://slack.com", c.Slack.APIURL)
}
