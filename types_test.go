package main

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestParseDueDate(t *testing.T) {
	tests := []struct {
		in      string
		out     time.Time
		wantErr bool
	}{
		{
			in:  "2020-11-22",
			out: time.Date(2020, 11, 22, 0, 0, 0, 0, time.UTC),
		},
		{
			in:      "22-11-2020",
			wantErr: true,
		},
	}
	for _, test := range tests {
		out, err := ParseDueDate(test.in)
		if test.wantErr {
			assert.Error(t, err)
			continue
		}
		assert.NoError(t, err)
		assert.Equal(t, test.out, out)
	}
}

func TestIssue_GetHumanizeDueDate(t *testing.T) {
	i := &Issue{}
	assert.Equal(t, "?", i.GetHumanizeDueDate())
	i = &Issue{
		Fields: Fields{
			DueDate: time.Now().Add(-time.Hour).Format("2006-01-02"),
		},
	}
	assert.NotEqual(t, "?", i.GetHumanizeDueDate())
	assert.Contains(t, i.GetHumanizeDueDate(), "ago")
}

func TestIssue_GetLabels(t *testing.T) {
	i := &Issue{}
	assert.Equal(t, "", i.GetLabels())
	i = &Issue{
		Fields: Fields{
			Labels: []string{
				"foo",
				"bar",
			},
		},
	}
	assert.Equal(t, " (bar, foo)", i.GetLabels())
	i = &Issue{
		Fields: Fields{
			Components: []*Component{
				{
					Name: "ABC",
				},
				{
					Name: "DEF",
				},
			},
			Labels: []string{
				"foo",
				"bar",
			},
		},
	}
	assert.Equal(t, " (ABC, DEF, bar, foo)", i.GetLabels())
}
