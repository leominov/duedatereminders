package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"

	"github.com/sirupsen/logrus"
)

type JiraRealClient struct {
	logger   *logrus.Entry
	endpoint string
	username string
	password string
	client   *http.Client
}

// SetCredentials to set up auth token
func (j *JiraRealClient) SetCredentials(username, password string) {
	j.username = username
	j.password = password
}

// Endpoint returns Stash endpoint
func (j *JiraRealClient) Endpoint() string {
	return j.endpoint
}

func (j *JiraRealClient) log(methodName string, args ...interface{}) {
	if j.logger == nil {
		return
	}
	var as []string
	for _, arg := range args {
		as = append(as, fmt.Sprintf("%v", arg))
	}
	j.logger.Infof("%s(%s)", methodName, strings.Join(as, ", "))
}

func (j *JiraRealClient) doRequest(method, path, accept string, body interface{}) (*http.Response, error) {
	var buf io.Reader
	if body != nil {
		b, err := json.Marshal(body)
		if err != nil {
			return nil, err
		}
		buf = bytes.NewBuffer(b)
	}
	req, err := http.NewRequest(method, path, buf)
	if err != nil {
		return nil, err
	}
	req.SetBasicAuth(j.username, j.password)
	req.Header.Add("Content-Type", "application/json")
	if accept != "" {
		req.Header.Add("Accept", accept)
	}
	req.Close = true
	return j.client.Do(req)
}

func (j *JiraRealClient) GetIssues(jql string) ([]*Issue, error) {
	j.log("GetIssues", jql)
	type ir struct {
		Total  int      `json:"total"`
		Issues []*Issue `json:"issues"`
	}
	path := fmt.Sprintf("%s/rest/api/2/search?jql=%s", j.endpoint, url.QueryEscape(jql))
	resp, err := j.doRequest(http.MethodGet, path, "", nil)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected response code: %s", resp.Status)
	}
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	irE := ir{}
	err = json.Unmarshal(b, &irE)
	if err != nil {
		return nil, err
	}
	return irE.Issues, nil
}
