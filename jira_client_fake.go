package main

type JiraFakeClient struct {
	hits   int
	issues []*Issue
	err    error
}

func (j *JiraFakeClient) SetCredentials(_, _ string) {}

func (j *JiraFakeClient) Endpoint() string {
	return "http://127.0.0.1:8080"
}

func (j *JiraFakeClient) GetIssues(_ string) ([]*Issue, error) {
	j.hits++
	return j.issues, j.err
}
