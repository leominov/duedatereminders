package main

import (
	"net/http"
	"strings"

	"github.com/sirupsen/logrus"
)

type JiraClient interface {
	SetCredentials(username, password string)
	Endpoint() string
	GetIssues(jql string) ([]*Issue, error)
}

// NewJiraClient returns JIRA client
func NewJiraClient(endpoint string, httpCli *http.Client) JiraClient {
	endpoint = strings.TrimRight(endpoint, "/")
	return &JiraRealClient{
		logger:   logrus.WithField("client", "jira"),
		client:   httpCli,
		endpoint: endpoint,
	}
}
