package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"github.com/robfig/cron/v3"

	"gopkg.in/yaml.v2"
)

const (
	defaultSlackUsername  = "DueDate Reminders"
	defaultSlackIconEmoji = ":tell_me:"
	defaultSlackFooter    = "leominov/DueDateReminders"
	defaultSlackColor     = "warning"
)

type Configuration struct {
	DryRun   bool        `yaml:"dry_run"`
	Schedule string      `yaml:"schedule"`
	Jira     JiraConfig  `yaml:"jira_config"`
	Slack    SlackConfig `yaml:"slack_config"`
	Template string      `yaml:"template"`
}

type JiraConfig struct {
	JQL      string `yaml:"jql"`
	Host     string `yaml:"host"`
	Username string `yaml:"username"`
	Password string `yaml:"password"`
}

type SlackConfig struct {
	SlackRequest `yaml:",inline"`
	Enabled      bool   `yaml:"enabled"`
	APIURL       string `yaml:"api_url"`
	Template     string `yaml:"template"`
	Footer       string `yaml:"footer"`
	Color        string `yaml:"color"`
}

func LoadConfigurationFromFile(filename string) (*Configuration, error) {
	b, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	c := &Configuration{}
	err = yaml.UnmarshalStrict(b, c)
	if err != nil {
		return nil, err
	}

	parser := cron.NewParser(
		cron.Second | cron.Minute | cron.Hour | cron.Dom | cron.Month | cron.Dow | cron.Descriptor,
	)
	_, err = parser.Parse(c.Schedule)
	if err != nil {
		return nil, fmt.Errorf("cron: %v", err)
	}

	c.Jira.Host = strings.TrimRight(c.Jira.Host, "/")
	if len(c.Slack.Username) == 0 {
		c.Slack.Username = defaultSlackUsername
	}
	if len(c.Slack.IconEmoji) == 0 {
		c.Slack.IconEmoji = defaultSlackIconEmoji
	}
	if len(c.Slack.Footer) == 0 {
		c.Slack.Footer = defaultSlackFooter
	}
	if len(c.Slack.Color) == 0 {
		c.Slack.Color = defaultSlackColor
	}
	return c, nil
}

func (c *Configuration) LoadSensitiveDataFromEnv() {
	if ju := os.Getenv("JIRA_USERNAME"); len(ju) > 0 {
		c.Jira.Username = ju
	}
	if jp := os.Getenv("JIRA_PASSWORD"); len(jp) > 0 {
		c.Jira.Password = jp
	}
	if sau := os.Getenv("SLACK_API_URL"); len(sau) > 0 {
		c.Slack.APIURL = sau
	}
}
